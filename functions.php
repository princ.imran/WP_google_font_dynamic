<?php
/**
 * industry functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package industry
 */



/**
 * Google Custom fonts
 */

if ( ! function_exists( 'industry_theme_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own industry_theme_fonts_url() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function industry_theme_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';	

	$body_font = cs_get_option('body_font');
	$body_font_variant_array = cs_get_option('body_font_variant');
	$body_font_variant = implode(',',$body_font_variant_array);

	$heading_font = cs_get_option('heading_font');
	$different_hadding_font = cs_get_option('different_hadding_font');
	$heading_font_variant_array = cs_get_option('heading_font_variant');
	$heading_font_variant = implode(',',$heading_font_variant_array);

	if(!empty($body_font)){
		$body_font_family = $body_font['family'];
	}else{
		$body_font_family = 'Montserrat';
	}

	if(!empty($heading_font)){
		$heading_font_family = $heading_font['family'];
	}else{
		$heading_font_family = 'Montserrat';
	}

	$fonts[] = ''.esc_attr($body_font_family).':'.esc_attr($body_font_variant).'';

if($different_hadding_font == true){
	$fonts[] = ''.esc_attr($heading_font_family).':'.esc_attr($heading_font_variant).'';
}


	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;



/**
 * Themeoptions and metabox functions.
 */
require(get_template_directory().'/inc/theme-metabox-and-options.php');

/**
 * Theme Style functions.
 */
require(get_template_directory().'/inc/theme-style.php');


