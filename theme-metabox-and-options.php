<?php


// framework Theme options filter example
function industry_theme_options( $options ) {

  $options      = array(); // remove old options

  $options[]    = array(
    'name'      => 'typography',
    'title'     => 'Typography',
    'icon'      => 'fa fa-heart',
    'fields'    => array(
      array(
        'id'    => 'body_font',
        'type'  => 'typography',
        'title' => 'Body Font',
        'default'   => array(
          'family'  => 'Montserrat',
          'variant' => '300',        
        ),
      ),
//Font Variant Add
      array(
        'id'    => 'body_font_variant',
        'type'  => 'checkbox',
        'title' => 'Body Font Variant',
        'options'  => array(
          '300'  => '300',
          '300i'   => '300 Italic',
          'regular' => '400',
          'italic'    => '400 Italic',
           '500'  => '500',
          '500i'   => '500 Italic',
           '600'  => '600',
          '600i'   => '600 Italic',
          '700'  => '700',
          '700i'   => '700 Italic',
          '800'  => '800',
          '800i'   => '800 Italic',
          '900'  => '900',
          '900i'   => '900 Italic',
        ),
        'default'  => array( '300', 'regular', '600', '800' ),
      ),

      array(
        'id'    => 'body_font_size',
        'type'  => 'text',
        'title' => 'Body Font Size',
        'default'   => '14px',        
        ),

      //Different Hading Font

       array(
        'id'    => 'different_hadding_font',
        'type'  => 'switcher',
        'title' => 'Different Heading Font',
        'default'   => 'false',        
        ),

      array(
        'id'    => 'heading_font',
        'type'  => 'typography',
        'title' => 'Heading Font',
        'default'   => array(
          'family'  => 'Montserrat',
          'variant' => '300',        
        ),
        'dependency' => array( 'different_hadding_font', '==', 'true' ) 
      ),            
            
          
       //Font Variant Add
      array(
        'id'    => 'heading_font_variant',
        'type'  => 'checkbox',
        'title' => 'Heading Font Variant',
        'options'  => array(
          '300'  => '300',
          '300i'   => '300 Italic',
          'regular' => '400',
          'italic'    => '400 Italic',
           '500'  => '500',
          '500i'   => '500 Italic',
           '600'  => '600',
          '600i'   => '600 Italic',
          '700'  => '700',
          '700i'   => '700 Italic',
          '800'  => '800',
          '800i'   => '800 Italic',
          '900'  => '900',
          '900i'   => '900 Italic',
        ),
        'default'  => array( '400', 'regular', '800' ),
        'dependency' => array( 'different_hadding_font', '==', 'true' ) 

      ),

      ),   

  );

  return $options;

}
add_filter( 'cs_framework_options', 'industry_theme_options' );

